#pragma once
#include "ArrFigura.h"


namespace POOFusion {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		ArrFigura* figuras;
		Graphics^ graphics;
		BufferedGraphics^ buffer;
		BufferedGraphicsContext^ context;

		MyForm(void)
		{
			InitializeComponent();
			graphics = this->CreateGraphics();
			context = BufferedGraphicsManager::Current;
			buffer = context->Allocate(graphics, this->ClientRectangle);
			figuras = new ArrFigura();

			for (int i = 0; i < 40; i++)
			{
				figuras->AgregarGoten();
				figuras->AgregarTrunks();
			}
			//
			//TODO: Add the constructor code her
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Timer^  timer;
	protected:

	protected:
	private: System::ComponentModel::IContainer^  components;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->timer = (gcnew System::Windows::Forms::Timer(this->components));
			this->SuspendLayout();
			// 
			// timer
			// 
			this->timer->Enabled = true;
			this->timer->Tick += gcnew System::EventHandler(this, &MyForm::timer_Tick);
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(600, 600);
			this->Name = L"MyForm";
			this->Text = L"MyForm";
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void timer_Tick(System::Object^  sender, System::EventArgs^  e)
	{
		buffer->Graphics->Clear(Color::White);
		figuras->MostrarFiguras(buffer->Graphics);
		figuras->MoverFiguras();

		buffer->Render(graphics);
	}
	};
}
