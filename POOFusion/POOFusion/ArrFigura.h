#pragma once
#include <stdio.h>

#include "Goten.h"
#include "Trunks.h"
#include "Gotenks.h"
#include <vcclr.h>

using namespace System;

class ArrFigura
{
private:
	gcroot<Random^> aleatorio;
public:
	Figura** arreglo;
	int cantidad;
	ArrFigura();
	~ArrFigura();
	void AgregarTrunks();
	void AgregarGoten();
	void AgregarGotenks(int indexGoten, int indexTrunks);
	void MostrarFiguras(Graphics^ graphics);
	void MoverFiguras();
	void EliminarFiguras();
};

ArrFigura::ArrFigura()
{
	arreglo = NULL;
	aleatorio = gcnew Random();
	cantidad = 0;
}

ArrFigura::~ArrFigura()
{
	delete[] arreglo;
}

void ArrFigura::AgregarTrunks()
{
	int xRandom = aleatorio->Next(500);
	int yRandom = aleatorio->Next(500);

	Figura** auxiliar = new Figura*[cantidad + 1];

	for (int i = 0; i < cantidad; i++)
		auxiliar[i] = arreglo[i];

	auxiliar[cantidad] = new Trunks(xRandom, yRandom);
	cantidad++;

	if (arreglo != NULL)
		delete arreglo;
	
	arreglo = auxiliar;
}
void ArrFigura::AgregarGoten()
{
	int xRandom = aleatorio->Next(500);
	int yRandom = aleatorio->Next(500);

	Figura** auxiliar = new Figura*[cantidad + 1];

	for (int i = 0; i < cantidad; i++)
		auxiliar[i] = arreglo[i];

	auxiliar[cantidad] = new Goten(xRandom, yRandom);
	cantidad++;

	if (arreglo != NULL)
		delete arreglo;

	arreglo = auxiliar;
}
void ArrFigura::AgregarGotenks(int indexGoten, int indexTrunks)
{
	int xGotenks = (arreglo[indexGoten]->x + arreglo[indexTrunks]->x) / 2;
	int yGotenks = (arreglo[indexGoten]->y + arreglo[indexTrunks]->y) / 2;

	arreglo[indexGoten]->sevaAEliminar = true;
	arreglo[indexGoten]->sevaAEliminar = true;

	Figura** auxiliar = new Figura*[cantidad + 1];

	for (int i = 0; i < cantidad; i++)
		auxiliar[i] = arreglo[i];

	auxiliar[cantidad] = new Gotenks(xGotenks, yGotenks);
	cantidad++;

	if (arreglo != NULL)
		delete arreglo;

	arreglo = auxiliar;
}


void ArrFigura::MostrarFiguras(Graphics^ graphics)
{
	for (int i = 0; i < cantidad; i++)
		arreglo[i]->Mostrar(graphics);
}

void ArrFigura::MoverFiguras()
{
	for (int i = 0; i < cantidad; i++)
	{
		arreglo[i]->indice += 1;

		if (arreglo[i]->indice == 5)
			arreglo[i]->indice = 0;

		arreglo[i]->Mover();

		if (arreglo[i]->x <= 0 || arreglo[i]->x + arreglo[i]->ancho >= 600)
			arreglo[i]->dx *= -1;
		if (arreglo[i]->y <= 0 || arreglo[i]->y + arreglo[i]->alto >= 600)
			arreglo[i]->dy *= -1;
	}

	Rectangle sayayinA;
	Rectangle sayayinB;

	for (int i = 0; i < cantidad; i++)
	{
		for (int j = i + 1; j < cantidad - 1; j++)
		{
			sayayinA = Rectangle(arreglo[i]->x, arreglo[i]->y, arreglo[i]->ancho, arreglo[i]->alto);
			sayayinB = Rectangle(arreglo[j]->x, arreglo[j]->y, arreglo[j]->ancho, arreglo[j]->alto);
		
			if (sayayinA.IntersectsWith(sayayinB) && arreglo[i]->y == arreglo[j]->y)
			{
				if ((arreglo[i]->tipo == sGoten && arreglo[j]->tipo == sTrunks) || (arreglo[i]->tipo == sTrunks && arreglo[j]->tipo == sGoten))
					AgregarGotenks(i, j);
			}
		}
	}

	EliminarFiguras();
		
}

void ArrFigura::EliminarFiguras()
{
	int contador = 0;

	for (int i = 0; i < cantidad; i++)
	{
		if (arreglo[i]->sevaAEliminar == true)
			contador++;
	}

	if (contador == 0)
		return;
	
	
	Figura** auxiliar = new Figura*[cantidad - contador];
	int aux = 0;
	for (int i = 0; i < cantidad; i++)
	{
		if (arreglo[i]->sevaAEliminar == false)
		{
			auxiliar[aux] = arreglo[i];
			aux++;
		}		
	}

	cantidad -= contador;

	delete arreglo;

	arreglo = auxiliar;
}