#pragma once
#include "Figura.h"

class Trunks : public Figura
{
public:
	Trunks(int pX, int pY);
	~Trunks();
	void Mostrar(Graphics^ graphics);
};

Trunks::Trunks(int pX, int pY)
{
	tipo = sTrunks;
	sevaAEliminar = false;
	x = pX;
	y = pY;
	y = pY;
	dx = 5;
	dy = 5;
	indice = 0;
	ancho = 48;
	alto = 48;
	anchoSprite = 52;
	altoSprite = 57;
}

Trunks::~Trunks()
{
}

void Trunks::Mostrar(Graphics^ graphics)
{
	Image^ imagen = Image::FromFile("Trunks.png");
	graphics->DrawImage(imagen, Rectangle(x, y, ancho, alto), Rectangle(indice * anchoSprite, 0, anchoSprite, altoSprite), GraphicsUnit::Pixel);
}