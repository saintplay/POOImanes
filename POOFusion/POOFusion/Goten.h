#pragma once
#include "Figura.h"

class Goten : public Figura
{
public:
	Goten(int pX, int pY);
	~Goten();
	void Mostrar(Graphics^ graphics);
};

Goten::Goten(int pX, int pY)
{
	tipo = sGoten;
	sevaAEliminar = false;
	x = pX;
	y = pY;
	y = pY;
	dx = 5;
	dy = 5;
	indice = 0;
	ancho = 48;
	alto = 48;
	anchoSprite = 52;
	altoSprite = 57;
}

Goten::~Goten()
{
}

void Goten::Mostrar(Graphics^ graphics)
{
	Image^ imagen = Image::FromFile("Goten.png");
	graphics->DrawImage(imagen, Rectangle(x, y, ancho, alto), Rectangle(indice * anchoSprite, 0, anchoSprite, altoSprite), GraphicsUnit::Pixel);
}