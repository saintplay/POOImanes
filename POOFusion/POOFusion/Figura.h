#pragma once
using namespace System::Drawing;

enum Sayayin{sGoten, sTrunks, sGotenks};

class Figura
{
public:
	Sayayin tipo;
	bool sevaAEliminar;
	//Coordenadas
	int x;
	int y;
	//Direcciones de movimiento
	int dx;
	int dy;
	//Dimensiones de la figura
	int alto;
	int ancho;
	//Sprite
	int indice;
	int anchoSprite;
	int altoSprite;
	//Metodos
	Figura();
	~Figura();
	virtual void Mostrar(Graphics^ graphics) abstract;
	void Mover();
};

Figura::Figura()
{
	
}

Figura::~Figura()
{
}

void Figura::Mover()
{
	x += dx;
	y += dy;
}