#pragma once
#include "Figura.h"

class Gotenks : public Figura
{
public:
	Gotenks(int pX, int pY);
	~Gotenks();
	void Mostrar(Graphics^ graphics);
};

Gotenks::Gotenks(int pX, int pY)
{
	tipo = sGotenks;
	sevaAEliminar = false;
	x = pX;
	y = pY;
	dx = 5;
	dy = 5;
	indice = 0;
	ancho = 48;
	alto = 48;
	anchoSprite = 56;
	altoSprite = 96;
}

Gotenks::~Gotenks()
{
}

void Gotenks::Mostrar(Graphics^ graphics)
{
	Image^ imagen = Image::FromFile("Gotenks.png");
	graphics->DrawImage(imagen, Rectangle(x, y, ancho, alto), Rectangle(indice * anchoSprite, 0, anchoSprite, altoSprite), GraphicsUnit::Pixel);
}